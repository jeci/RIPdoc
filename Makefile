

build-docker:
	docker build -t jeci/ripdoc -f Docker/Dockerfile .

run-docker:
	docker run --rm --user "$(id -u):$(id -g)"\
		-v "/etc/passwd:/etc/passwd:ro" \
		-v "/etc/group:/etc/group:ro" \
		-v "$(PWD)/doc:/work" -w /work jeci/ripdoc
